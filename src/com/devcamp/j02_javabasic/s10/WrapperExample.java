package com.devcamp.j02_javabasic.s10;

public class WrapperExample {
    Byte myByte; // 1 byte whole num -128 to 127
    Short myShortNum; // 2 bytes whole num -32,768 to 32,767
    Integer myIntNum; // 4 bytes integer (whole number)
    Long myLongNum; // 8 bytes
    Float myFloatNum; // 4 bytes 6 to 7 decimal digits (floating point number)
    Double myDoubleNum; // 8 bytes 15 decimal digits
    Boolean myBool; // 1 bit boolean
    Character myLetter; // 2 bytes character

    //khởi tạo không có tham số
    public WrapperExample() {
        myByte = 1; 
        myShortNum = 11;
        myIntNum = 111;
        myLongNum = (long) 1111;
        myFloatNum = 11.11f; 
        myDoubleNum = 101.101d;
        myBool = true; 
        myLetter = 'A'; 
    }
    // khởi tạo có tất cả 8 tham số
    public WrapperExample(Byte ByteNum, Short ShortNum, Integer IntNum, Long LongNum,
                    Float FloatNum, Double DoubleNum, Boolean Bool, Character Letter) {
        myByte = ByteNum;
        myShortNum = ShortNum;
        myIntNum = IntNum;
        myLongNum = LongNum;
        myFloatNum = FloatNum;
        myDoubleNum = DoubleNum;
        myBool = Bool;
        myLetter = Letter;
    }

    public static void main(String[] args) throws Exception {
        System.out.println("Run 1");
        WrapperExample customer = new WrapperExample();
        customer.showInfo();

        System.out.println("Run 2");
        customer = new WrapperExample((byte) 2, (short) 22, 222, (long) 2222, 22.22f, 222.222d, false, 'B');
        customer.showInfo();

        System.out.println("Run 3");
        customer = new WrapperExample((byte) 3, (short) 33, 333, (long) 3333, 33.33f, 303.303d, true, 'C');
        customer.showInfo();

        System.out.println("Run 4");
        customer = new WrapperExample((byte) 4, (short) 44, 444, (long) 4444, 44.44f, 404.404d, false, 'D');
        customer.showInfo();

        System.out.println("Run 5");
        customer = new WrapperExample((byte) 5, (short) 55, 555, (long) 5555, 55.55f, 505.505, true, 'E');
        customer.showInfo(); 
    }
    public void showInfo() {
        System.out.printf("myByte=" + this.myByte); 
        System.out.printf(", myShortNum=" + this.myShortNum);
        System.out.printf(", myIntNum=" + this.myIntNum);
        System.out.printf(", myLongNum=" + this.myLongNum);
        System.out.printf(", myFloatNum=" + this.myFloatNum); 
        System.out.printf(", myDoubleNum=" + this.myDoubleNum);
        System.out.printf(", myBool=" + this.myBool); 
        System.out.println(", myLetter=" + this.myLetter); 
    }
}
